package sample;

import animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;


public class ConverterController {

    @FXML
    private ComboBox<String> ComboBox1;

    @FXML
    private ComboBox<String> ComboBox2;

    @FXML
    private TextField initialValueLength;

    @FXML
    private TextField resultLength;

    @FXML
    private TextField initialValueMassa;

    @FXML
    private ComboBox<String> ComboBox3;

    @FXML
    private ComboBox<String> ComboBox4;

    @FXML
    private TextField resultMassa;

    /**
     * Метод, для реализации очищения текстовых полей
     *
     * @param event нажатие на кнопку Очистить
     */
    @FXML
    void clearLength(ActionEvent event) {
        initialValueLength.clear();
        resultLength.clear();
    }

    /**
     * Метод, для реализации очищения текстовых полей
     *
     * @param event нажатие на кнопку Очистить
     */
    @FXML
    void clearMassa(ActionEvent event) {
        initialValueMassa.clear();
        resultMassa.clear();
    }

    /**
     * Сохраняет введенные в текстовое поле значения
     *
     * @param initialValueLength текст из текстового поля
     * @return число, если введены нечисловые данные то 0
     */
    private double outPutErrorLength(TextField initialValueLength) {
        try {
            return Double.parseDouble(initialValueLength.getText());
        } catch (Exception e) {
            initialValueLength.setText("Введите число");
            initialValueLength.setStyle("-fx-text-fill: red;");
        }
        return 0;
    }

    /**
     * Метод, для выполнения конвертации длины
     *
     * @param event нажатие на кнопку Вычислить
     */
    @FXML
    void convertLength(ActionEvent event) {
        if (initialValueLength.getText().isEmpty()) {
            Shake valueShake = new Shake(initialValueLength);
            valueShake.playAnimation();
            return;
        }
        String box1 = ComboBox1.getValue();
        String box2 = ComboBox2.getValue();
        if (box1.equals("Миллиметры (мм)") && box2.equals("Миллиметры (мм)")) {
            double millimeter = outPutErrorLength(initialValueLength);

            resultLength.setText(String.valueOf(millimeter));
        } else if (box1.equals("Миллиметры (мм)") && box2.equals("Сантиметры (см)")) {
            double millimeter = outPutErrorLength(initialValueLength);

            double centimeter = millimeter / 10;

            resultLength.setText(String.valueOf(centimeter));
        } else if (box1.equals("Миллиметры (мм)") && box2.equals("Дециметры (дм)")) {
            double millimeter = outPutErrorLength(initialValueLength);

            double decimeter = millimeter / 100;

            resultLength.setText(String.valueOf(decimeter));
        } else if (box1.equals("Миллиметры (мм)") && box2.equals("Метры (м)")) {
            double millimeter = outPutErrorLength(initialValueLength);

            double metre = millimeter / 1000;
            resultLength.setText(String.valueOf(metre));
        } else if (box1.equals("Миллиметры (мм)") && box2.equals("Километры (км)")) {
            double millimeter = outPutErrorLength(initialValueLength);

            double kilometer = millimeter / 10000;
            resultLength.setText(String.valueOf(kilometer));
        }
//------------------------------------------------------------------------------
        if (box1.equals("Сантиметры (см)") && box2.equals("Миллиметры (мм)")) {
            double centimeter = outPutErrorLength(initialValueLength);

            double millimeter = centimeter * 10;
            resultLength.setText(String.valueOf(millimeter));
        } else if (box1.equals("Сантиметры (см)") && box2.equals("Сантиметры (см)")) {
            double centimeter = outPutErrorLength(initialValueLength);

            resultLength.setText(String.valueOf(centimeter));
        } else if (box1.equals("Сантиметры (см)") && box2.equals("Дециметры (дм)")) {
            double centimeter = outPutErrorLength(initialValueLength);

            double decimeter = centimeter / 10;

            resultLength.setText(String.valueOf(decimeter));
        } else if (box1.equals("Сантиметры (см)") && box2.equals("Метры (м)")) {
            double centimeter = outPutErrorLength(initialValueLength);

            double metre = centimeter / 100;
            resultLength.setText(String.valueOf(metre));
        } else if (box1.equals("Сантиметры (см)") && box2.equals("Километры (км)")) {
            double centimeter = outPutErrorLength(initialValueLength);

            double kilometer = centimeter / 1000;
            resultLength.setText(String.valueOf(kilometer));
        }
//------------------------------------------------------------------------------
        if (box1.equals("Метры (м)") && box2.equals("Миллиметры (мм)")) {
            double metre = outPutErrorLength(initialValueLength);

            double millimeter = metre * 1000;
            resultLength.setText(String.valueOf(millimeter));
        } else if (box1.equals("Метры (м)") && box2.equals("Сантиметры (см)")) {
            double metre = outPutErrorLength(initialValueLength);

            double centimeter = metre * 100;
            resultLength.setText(String.valueOf(centimeter));
        } else if (box1.equals("Метры (м)") && box2.equals("Дециметры (дм)")) {
            double metre = outPutErrorLength(initialValueLength);

            double decimeter = metre * 10;
            resultLength.setText(String.valueOf(decimeter));
        } else if (box1.equals("Метры (м)") && box2.equals("Метры (м)")) {
            double metre = outPutErrorLength(initialValueLength);

            resultLength.setText(String.valueOf(metre));
        } else if (box1.equals("Метры (м)") && box2.equals("Километры (км)")) {
            double metre = outPutErrorLength(initialValueLength);

            double kilometer = metre / 100;
            resultLength.setText(String.valueOf(kilometer));
        }
//------------------------------------------------------------------------------
        if (box1.equals("Километры (км)") && box2.equals("Миллиметры (мм)")) {
            double kilometer = outPutErrorLength(initialValueLength);

            double millimeter = kilometer * 1000000;

            resultLength.setText(String.valueOf(millimeter));
        } else if (box1.equals("Километры (км)") && box2.equals("Сантиметры (см)")) {
            double kilometer = outPutErrorLength(initialValueLength);

            double centimeter = kilometer * 100000;
            resultLength.setText(String.valueOf(centimeter));
        } else if (box1.equals("Километры (км)") && box2.equals("Дециметры (дм)")) {
            double kilometer = outPutErrorLength(initialValueLength);

            double decimeter = kilometer * 10000;
            resultLength.setText(String.valueOf(decimeter));
        } else if (box1.equals("Километры (км)") && box2.equals("Метры (м)")) {
            double kilometer = outPutErrorLength(initialValueLength);

            double metre = kilometer * 1000;
            resultLength.setText(String.valueOf(metre));
        } else if (box1.equals("Километры (км)") && box2.equals("Километры (км)")) {
            double kilometer = outPutErrorLength(initialValueLength);

            resultLength.setText(String.valueOf(kilometer));
        }
    }

    /**
     * Сохраняет введенные в текстовое поле значения
     *
     * @param initialValueMassa текст из текстового поля
     * @return число, если введены нечисловые данные то 0
     */
    private double outPutErrorMassa(TextField initialValueMassa) {
        try {
            return Double.parseDouble(initialValueMassa.getText());
        } catch (Exception e) {
            initialValueMassa.setText("Введите число");
            initialValueMassa.setStyle("-fx-text-fill: red;");
        }
        return 0;
    }

    /**
     * Метод, для выполнения конвертации массы
     *
     * @param event нажатие на кнопку Вычислить
     */
    @FXML
    void convertMassa(ActionEvent event) {
        if (initialValueMassa.getText().isEmpty()) {
            Shake valueShake = new Shake(initialValueMassa);
            valueShake.playAnimation();
            return;
        }
        String box3 = ComboBox3.getValue();
        String box4 = ComboBox4.getValue();
//------------------------------------------------------------------------------
        if (box3.equals("Тонна") && box4.equals("Тонна")) {
            double tonne = outPutErrorMassa(initialValueMassa);

            resultMassa.setText(String.valueOf(tonne));
        } else if (box3.equals("Тонна") && box4.equals("Килограмм")) {
            double tonne = outPutErrorMassa(initialValueMassa);

            double kilogram = tonne * 1000;

            resultMassa.setText(String.valueOf(kilogram));
        } else if (box3.equals("Тонна") && box4.equals("Грамм")) {
            double tonne = outPutErrorMassa(initialValueMassa);

            double kilogram = tonne * 1e+6;

            resultMassa.setText(String.valueOf(kilogram));
        }
//------------------------------------------------------------------------------
        if (box3.equals("Килограмм") && box4.equals("Тонна")) {
            double kilogram = outPutErrorMassa(initialValueMassa);

            double tonne = kilogram / 1000;

            resultMassa.setText(String.valueOf(tonne));
        } else if (box3.equals("Килограмм") && box4.equals("Килограмм")) {
            double kilogram = outPutErrorMassa(initialValueMassa);

            resultMassa.setText(String.valueOf(kilogram));
        } else if (box3.equals("Килограмм") && box4.equals("Грамм")) {
            double kilogram = outPutErrorMassa(initialValueMassa);

            double gram = kilogram * 1000;

            resultMassa.setText(String.valueOf(gram));
        }

//------------------------------------------------------------------------------
        if (box3.equals("Грамм") && box4.equals("Тонна")) {
            double gram = outPutErrorMassa(initialValueMassa);

            double tonne = gram / 1e+6;

            resultMassa.setText(String.valueOf(tonne));
        } else if (box3.equals("Грамм") && box4.equals("Килограмм")) {
            double gram = outPutErrorMassa(initialValueMassa);

            double kilogram = gram / 1000;

            resultMassa.setText(String.valueOf(kilogram));
        } else if (box3.equals("Грамм") && box4.equals("Грамм")) {
            double gram = outPutErrorMassa(initialValueMassa);

            resultMassa.setText(String.valueOf(gram));
        }
    }

    @FXML
    void initialize() {
    }
}


